This module add permission for access to media settings pages.

So after enabling this module, if a role is set to "Administer media" it doesn't automatically get permission to "Administer media settings".

Author
------
Diego Ruiz "diego21"